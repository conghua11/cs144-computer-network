#include "tcp_receiver.hh"

using namespace std;

void TCPReceiver::receive( TCPSenderMessage message, Reassembler& reassembler, Writer& inbound_stream )
{
  if(!_syn_flag){
    if(!message.SYN){
      return;
    }
    _syn_flag = true;
    _isn = message.seqno;
  }
  if(message.FIN){
    _fin_flag = true;
  }
  uint64_t check_point = inbound_stream.bytes_pushed() + 1;
  uint64_t idx = Wrap32(message.seqno).unwrap(_isn, check_point);
  uint64_t first_idx = idx + message.SYN - 1;
  reassembler.insert(first_idx, message.payload, message.FIN, inbound_stream);
}

TCPReceiverMessage TCPReceiver::send( const Writer& inbound_stream ) const
{
  // Your code here.
  TCPReceiverMessage tcp_msg;
  uint64_t capacity = inbound_stream.available_capacity();
  tcp_msg.window_size = capacity > UINT16_MAX ? UINT16_MAX : capacity;
  if(!_syn_flag){
    tcp_msg.ackno = nullopt;
  } else {
    uint64_t check_point = inbound_stream.writer().bytes_pushed() + 1;
    if(_fin_flag & inbound_stream.writer().is_closed()){
      ++check_point;
    }
    tcp_msg.ackno = _isn + check_point;
  }
  return tcp_msg;
}
