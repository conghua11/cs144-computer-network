#pragma once

#include "address.hh"
#include "ethernet_frame.hh"
#include "ipv4_datagram.hh"

#include <iostream>
#include <list>
#include <optional>
#include <queue>
#include <unordered_map>
#include <utility>
#include <map>

// A "network interface" that connects IP (the internet layer, or network layer)
// with Ethernet (the network access layer, or link layer).

// This module is the lowest layer of a TCP/IP stack
// (connecting IP with the lower-layer network protocol,
// e.g. Ethernet). But the same module is also used repeatedly
// as part of a router: a router generally has many network
// interfaces, and the router's job is to route Internet datagrams
// between the different interfaces.

// The network interface translates datagrams (coming from the
// "customer," e.g. a TCP/IP stack or router) into Ethernet
// frames. To fill in the Ethernet destination address, it looks up
// the Ethernet address of the next IP hop of each datagram, making
// requests with the [Address Resolution Protocol](\ref rfc::rfc826).
// In the opposite direction, the network interface accepts Ethernet
// frames, checks if they are intended for it, and if so, processes
// the the payload depending on its type. If it's an IPv4 datagram,
// the network interface passes it up the stack. If it's an ARP
// request or reply, the network interface processes the frame
// and learns or replies as necessary.
class NetworkInterface
{
private:
  // Ethernet (known as hardware, network-access, or link-layer) address of the interface
  EthernetAddress ethernet_address_;

  // IP (known as Internet-layer or network-layer) address of the interface
  Address ip_address_;

  // My parameters
  static constexpr size_t MAX_CACHE_TIME = 30000;
  static constexpr size_t MAX_RETX_WAITING_TIME = 5000;

  struct Dgram_nextHop {
    InternetDatagram dgram;
    // size_t time_since_last_ARP_request_send = 0;
  };

  struct EtherAddress_Time {
    EthernetAddress mac_addr;
    size_t time;
  };

  std::queue<EthernetFrame> _frames_out{};
  std::map<uint32_t, std::queue<Dgram_nextHop>> _frames_track{};
  std::map<uint32_t, EtherAddress_Time> _ip_eth_map{};
  std::map<uint32_t, size_t> _ip_msTime_map{};

  size_t _ms_clock{0};

public:
  // Construct a network interface with given Ethernet (network-access-layer) and IP (internet-layer)
  // addresses
  NetworkInterface( const EthernetAddress& ethernet_address, const Address& ip_address );

  // Access queue of Ethernet frames awaiting transmission
  std::optional<EthernetFrame> maybe_send();

  // Sends an IPv4 datagram, encapsulated in an Ethernet frame (if it knows the Ethernet destination
  // address). Will need to use [ARP](\ref rfc::rfc826) to look up the Ethernet destination address
  // for the next hop.
  // ("Sending" is accomplished by making sure maybe_send() will release the frame when next called,
  // but please consider the frame sent as soon as it is generated.)
  void send_datagram( const InternetDatagram& dgram, const Address& next_hop );

  // Receives an Ethernet frame and responds appropriately.
  // If type is IPv4, returns the datagram.
  // If type is ARP request, learn a mapping from the "sender" fields, and send an ARP reply.
  // If type is ARP reply, learn a mapping from the "sender" fields.
  std::optional<InternetDatagram> recv_frame( const EthernetFrame& frame );

  // Called periodically when time elapses
  void tick( size_t ms_since_last_tick );

  // helper function
  void send_arp(uint16_t opcode, EthernetAddress &dst, uint32_t dst_ip);
};

// Network interface timer
class NITimer
{
private:
  uint64_t ticks_ = 0;
  bool running = false;
public:
  bool expired(uint64_t ms_since_last_tick, uint64_t time_out_) {
      ticks_ += ms_since_last_tick;
      return running && (ticks_ >= time_out_);
  }
  uint64_t tick_now() { return ticks_; }
  bool in_run() { return running; }
  void start() {
      ticks_ = 0;
      running = true;
  }
  void stop() { running = false; }
};