#include "reassembler.hh"

using namespace std;

void Reassembler::insert( uint64_t first_index, string data, bool is_last_substring, Writer& output )
{
  if ( !data.empty() ) {
    preprocess( first_index, data, output );
  }
  // make sure the buff
  for(auto it = _buf.begin(); !_buf.empty() && it != _buf.end();){
    if ( it->first + it->second.length() < nxt_idx ) {
      _buf.erase( it++ );
      continue;
    }
    if ( it->first > nxt_idx ) {
      break;
    }
    if ( it->first == nxt_idx ) {
      unassembled_bytes -= it->second.size();
      output.push( it->second );
      nxt_idx += it->second.length();
      _buf.erase( it++ );
    }
  }
  if ( is_last_substring ) {
    end_checked = true;
    eof_idx = first_index + data.size();
  }
  if ( end_checked && output.writer().bytes_pushed() == eof_idx ) {
    output.close();
  }
}

void Reassembler::preprocess( uint64_t& first_index, std::string& data, Writer& output )
{
  uint64_t cap = output.available_capacity();
  nxt_idx = output.bytes_pushed();
  uint64_t max_idx = output.reader().bytes_popped() + output.reader().bytes_buffered() + cap;
  if ( nxt_idx == max_idx ) {
    output.close();
    return;
  }
  if ( first_index >= max_idx || first_index + data.size() - 1 < nxt_idx ) {
    return;
  }
  // need to cut the end
  if ( first_index + data.size() > max_idx ) {
    data = data.substr( 0, max_idx - first_index );
  }
  // need to cut off the front
  if ( first_index < nxt_idx && first_index + data.size() > nxt_idx ) {
    data = data.substr( nxt_idx - first_index );
    first_index = nxt_idx;
  }
  if ( _buf.empty() ) {
    unassembled_bytes += data.size();
    _buf[first_index] = data;
    return;
  }
  store( first_index, data );
}

void Reassembler::store( uint64_t str_nxt_idx, std::string str )
{
  for ( map<uint64_t, std::string>::iterator it = _buf.begin(); it != _buf.end();) {
    size_t str_end = str_nxt_idx + str.size() - 1;
    size_t it_end = it->first + it->second.size() - 1;

    if ( ( str_nxt_idx >= it->first && str_nxt_idx <= it_end )
         || ( it->first >= str_nxt_idx && it->first <= str_end ) || ( str_end + 1 == it->first ) ) {
      merge( str_nxt_idx, str, it->first, it->second );
      unassembled_bytes -= it->second.size();
      _buf.erase( it++ );
    } else {
      ++it;
    }
  }
  unassembled_bytes += str.size();
  _buf[str_nxt_idx] = str;
}

void Reassembler::merge( uint64_t& idx, string& str, uint64_t bufidx, string bufstr )
{
  size_t seg_tail = idx + str.length();
  size_t cache_tail = bufidx + bufstr.length();
  if ( seg_tail + 1 == 1 ) {
    str = str + bufstr;
    return;
  }
  // str front bufstr back
  if ( idx < bufidx && seg_tail <= cache_tail ) {
    str = str.substr( 0, bufidx - idx ) + bufstr;
    return;
  }
  // bufstr front str back
  else if ( idx >= bufidx && seg_tail > cache_tail ) {
    str = bufstr + str.substr( bufidx + bufstr.length() - idx );
    idx = bufidx;
    return;
  }
  // str is covered by buf str
  else if ( idx >= bufidx && seg_tail <= cache_tail ) {
    str = bufstr;
    idx = bufidx;
    return;
  }
}

uint64_t Reassembler::bytes_pending() const
{
  // Your code here.
  return unassembled_bytes;
}
