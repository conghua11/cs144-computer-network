#include "network_interface.hh"

#include "arp_message.hh"
#include "ethernet_frame.hh"

using namespace std;

// ethernet_address: Ethernet (what ARP calls "hardware") address of the interface
// ip_address: IP (what ARP calls "protocol") address of the interface
NetworkInterface::NetworkInterface( const EthernetAddress& ethernet_address, const Address& ip_address )
  : ethernet_address_( ethernet_address ), ip_address_( ip_address )
{
  cerr << "DEBUG: Network interface has Ethernet address " << to_string( ethernet_address_ ) << " and IP address "
       << ip_address.ip() << "\n";
}

// dgram: the IPv4 datagram to be sent
// next_hop: the IP address of the interface to send it to (typically a router or default gateway, but
// may also be another host if directly connected to the same network as the destination)

// Note: the Address type can be converted to a uint32_t (raw 32-bit IP address) by using the
// Address::ipv4_numeric() method.
void NetworkInterface::send_datagram( const InternetDatagram& dgram, const Address& next_hop )
{
  uint32_t ip_numeric = next_hop.ipv4_numeric();
  if ( _ip_eth_map.count( ip_numeric )  ) {
    if ( _ms_clock - _ip_eth_map[ip_numeric].time >= MAX_CACHE_TIME ) {
      _ip_eth_map.erase( ip_numeric );
      _frames_track[ip_numeric].push( Dgram_nextHop { dgram} );
      send_arp( ARPMessage::OPCODE_REQUEST, const_cast<EthernetAddress&>( ETHERNET_BROADCAST ), ip_numeric );
      _ip_msTime_map[ip_numeric];
      return;
    }

    EthernetFrame frame;
    frame.header.dst = _ip_eth_map[ip_numeric].mac_addr;
    frame.header.src = ethernet_address_;
    frame.header.type = EthernetHeader::TYPE_IPv4;

    frame.payload = serialize( dgram );
    _frames_out.push( frame );
    return;
  }

  if ( !_ip_msTime_map.count( ip_numeric ) ) {
    _frames_track[ip_numeric].push( Dgram_nextHop { dgram } );
    send_arp( ARPMessage::OPCODE_REQUEST, const_cast<EthernetAddress&>( ETHERNET_BROADCAST ), ip_numeric );
    _ip_msTime_map[ip_numeric];
  } else if ( _ms_clock - _ip_msTime_map[ip_numeric] >= MAX_RETX_WAITING_TIME ) {
    send_arp( ARPMessage::OPCODE_REQUEST, const_cast<EthernetAddress&>( ETHERNET_BROADCAST ), ip_numeric );
    _ip_msTime_map.erase( ip_numeric );
  }
}

void NetworkInterface::send_arp( uint16_t opcode, EthernetAddress& dst, uint32_t dst_ip )
{
  ARPMessage arp;
  EthernetFrame frame;
  arp.opcode = opcode;
  arp.sender_ethernet_address = ethernet_address_;
  arp.sender_ip_address = ip_address_.ipv4_numeric();
  // ARP reply
  if ( opcode != ARPMessage::OPCODE_REQUEST ) {
    arp.target_ethernet_address = dst;
  }
  arp.target_ip_address = dst_ip;

  frame.header.src = ethernet_address_;
  frame.header.dst = dst;
  frame.header.type = EthernetHeader::TYPE_ARP;

  frame.payload = serialize( arp );

  _frames_out.emplace( frame );
}

// frame: the incoming Ethernet frame
optional<InternetDatagram> NetworkInterface::recv_frame( const EthernetFrame& frame )
{
  if ( frame.header.dst != ethernet_address_ && frame.header.dst != ETHERNET_BROADCAST ) {
    return nullopt;
  }
  ARPMessage arp_msg;
  InternetDatagram dgram;
  switch ( frame.header.type ) {
    case EthernetHeader::TYPE_ARP:
      
      parse( arp_msg, frame.payload );
      if ( arp_msg.opcode == arp_msg.OPCODE_REQUEST
        && arp_msg.target_ip_address == ip_address_.ipv4_numeric()) {
        send_arp( ARPMessage::OPCODE_REPLY, arp_msg.sender_ethernet_address, arp_msg.sender_ip_address );
        _ip_eth_map[arp_msg.sender_ip_address] = { arp_msg.sender_ethernet_address, _ms_clock };
        return nullopt;
      }
      // arp reply
      _ip_eth_map[arp_msg.sender_ip_address] = { arp_msg.sender_ethernet_address, _ms_clock };

      while (_frames_track[arp_msg.sender_ip_address].size()) {
        send_datagram( _frames_track[arp_msg.sender_ip_address].front().dgram, Address::from_ipv4_numeric(arp_msg.sender_ip_address));
        _frames_track[arp_msg.sender_ip_address].pop();
      }
      break;
    case EthernetHeader::TYPE_IPv4:
      parse( dgram, frame.payload );
      return dgram;
      break;
    default:
      break;
  }
  return nullopt;
}

// ms_since_last_tick: the number of milliseconds since the last call to this method
void NetworkInterface::tick( const size_t ms_since_last_tick )
{
  _ms_clock += ms_since_last_tick;
}

optional<EthernetFrame> NetworkInterface::maybe_send()
{
  if ( _frames_out.empty() ) {
    return {};
  }
  optional<EthernetFrame> frame = _frames_out.front();
  _frames_out.pop();
  return frame;

}
