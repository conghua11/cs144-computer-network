#include "wrapping_integers.hh"

using namespace std;

Wrap32 Wrap32::wrap( uint64_t n, Wrap32 zero_point )
{
  // Your code here.
  Wrap32 result{ (uint32_t)((n + (uint64_t)zero_point.raw_value_) & 0x00000000ffffffff )};
  return result;
}

uint64_t Wrap32::unwrap( Wrap32 zero_point, uint64_t checkpoint ) const
{
  // Your code here.
    const constexpr uint64_t INT32_RANGE = 1l << 32;
    // 获取 n 与 isn 之间的偏移量（mod）
    // 实际的 absolute seqno % INT32_RANGE == offset
    uint32_t offset = this->raw_value_ - zero_point.raw_value_;
    /// NOTE: 最大的坑点！如果 checkpoint 比 offset 大，那么就需要进行四舍五入
    /// NOTE: 但是!!! 如果 checkpoint 比 offset 还小，那就只能向上入了，即此时的 offset 就是 abs seqno
    if(checkpoint > offset) {
        // 加上半个 INT32_RANGE 是为了四舍五入
        uint64_t real_checkpoint = (checkpoint - offset) + (INT32_RANGE >> 1);
          
        return (real_checkpoint & 0xffffffff00000000) + offset;
    }
    else
       return offset;
}
