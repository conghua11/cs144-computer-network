#include "tcp_sender.hh"
#include "tcp_config.hh"
#include "tcp_sender_message.hh"

#include <random>

using namespace std;

/* TCPSender constructor (uses a random ISN if none given) */
TCPSender::TCPSender( uint64_t initial_RTO_ms, optional<Wrap32> fixed_isn )
  : isn_( fixed_isn.value_or( Wrap32 { random_device()() } ) )
  , initial_RTO_ms_( initial_RTO_ms )
{}

uint64_t TCPSender::sequence_numbers_in_flight() const
{
  // Your code here.
  return bytes_in_flight_;
}

uint64_t TCPSender::consecutive_retransmissions() const
{
  // Your code here.
  return consecutive_retransmissions_;
}

optional<TCPSenderMessage> TCPSender::maybe_send()
{
  // Your code here.
  if(_segments_out.empty()){
    return {};
  }
  if(consecutive_retransmissions_ && timer.in_run() && timer.tick_now() > 0) {
    return {};
  }
  TCPSenderMessage msg = _segments_out.front();
  _segments_out.pop();
  return msg;
}

void TCPSender::push( Reader& outbound_stream )
{
  // Your code here.
  if(fin_sent_){
    return;
  }
  uint64_t win_size = _ackno + (_window_size ? _window_size : 1) - _next_seqno;
  while(win_size > 0 && !fin_sent_){
    TCPSenderMessage msg;
    if(!_next_seqno){
      msg.SYN = true;
      syn_sent_ = true;
      win_size--;
    }
    msg.seqno = Wrap32::wrap(_next_seqno, isn_);
    Buffer &buffer = msg.payload;
    read(outbound_stream, min(win_size, TCPConfig::MAX_PAYLOAD_SIZE), buffer);
    win_size -= buffer.size();
    if(outbound_stream.is_finished() && win_size > 0){
      msg.FIN = true;
      fin_sent_ = true;
      --win_size;
    }
    uint64_t len = msg.sequence_length();
    if(!len) {
      return;
    }
    _segments_out.emplace(msg);
    if(!timer.in_run()){
      timer.start();
    }
    _segments_track.emplace(msg);
    _next_seqno += len;
    bytes_in_flight_ += len;
  }
}

TCPSenderMessage TCPSender::send_empty_message() const
{
  // Your code here.
  TCPSenderMessage msg;
  msg.seqno = Wrap32::wrap(_next_seqno, isn_);
  return msg;
}

void TCPSender::receive( const TCPReceiverMessage& msg )
{
  // Your code here.

  if(msg.ackno){
    _ackno = msg.ackno->unwrap(isn_, _next_seqno);
  }
  if(_ackno > _next_seqno){
    return;
  }

  _window_size = msg.window_size;
  bool new_check_;
  // 接收成功

  while(!_segments_track.empty()){
    TCPSenderMessage send_msg = _segments_track.front();
    uint64_t len = send_msg.sequence_length();
    uint64_t seqno = send_msg.seqno.unwrap(isn_, _next_seqno);
    if(seqno + len > _ackno) {
      break;
    }
    _segments_track.pop();
    bytes_in_flight_ -= len;
    new_check_ = true;
  }
  if(new_check_){
    retransmission_timeout_ = initial_RTO_ms_;
    if(_segments_track.empty())
      timer.stop();
    else {
      timer.start();
    }
    consecutive_retransmissions_ = 0;
  }
}

void TCPSender::tick( const size_t ms_since_last_tick )
{
  // Your code here.
  if(!timer.expired(ms_since_last_tick, retransmission_timeout_) || 
    _segments_track.empty()) {
      return;
  }
  _segments_out.push(_segments_track.front());
  if(_window_size != 0) {
    retransmission_timeout_ <<= 1;
  }
  ++consecutive_retransmissions_;
  maybe_send();
  timer.start();
}