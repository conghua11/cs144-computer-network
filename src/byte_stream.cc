#include <stdexcept>
#include <string_view>

#include "byte_stream.hh"

using namespace std;

ByteStream::ByteStream( uint64_t capacity ) : capacity_( capacity ) {}

void Writer::push( string data )
{
  // Your code here.
  size_t len = data.length();
  if (len > capacity_ - _buf.size()) {
    len = capacity_ - _buf.size();
  }
  _write_count += len;
  for(size_t i = 0; i < len; i++){
    _buf.push_back(data[i]);
  }
}

void Writer::close()
{
  // Your code here.
  _closed = true;
}

void Writer::set_error()
{
  // Your code here.
  _error = true;
}

bool Writer::is_closed() const
{
  // Your code here.
  return _closed;
}

uint64_t Writer::available_capacity() const
{
  // Your code here.
  return capacity_ - _buf.size();
}

uint64_t Writer::bytes_pushed() const
{
  // Your code here.
  return _write_count;
}

string_view Reader::peek() const
{
  // Your code here.
  return {std::string_view(&_buf.front(), 1)};
}

bool Reader::is_finished() const
{
  // Your code here.
  return _buf.empty() && _closed;
}

bool Reader::has_error() const
{
  // Your code here.
  return _error ? true:false;
}

void Reader::pop( uint64_t len )
{
  // Your code here.
  if (len > _buf.size()){
    len = _buf.size();
  }
  _read_count += len;
  for(uint64_t i = 0; i<len; i++){
    _buf.pop_front();
  }
}

uint64_t Reader::bytes_buffered() const
{
  // Your code here.
  return _buf.size();
}

uint64_t Reader::bytes_popped() const
{
  // Your code here.
  return _read_count;
}
