#include "router.hh"

#include <iostream>
#include <limits>

using namespace std;

// route_prefix: The "up-to-32-bit" IPv4 address prefix to match the datagram's destination address against
// prefix_length: For this route to be applicable, how many high-order (most-significant) bits of
//    the route_prefix will need to match the corresponding bits of the datagram's destination address?
// next_hop: The IP address of the next hop. Will be empty if the network is directly attached to the router (in
//    which case, the next hop address should be the datagram's final destination).
// interface_num: The index of the interface to send the datagram out on.
void Router::add_route( const uint32_t route_prefix,
                        const uint8_t prefix_length,
                        const optional<Address> next_hop,
                        const size_t interface_num )
{
  cerr << "DEBUG: adding route " << Address::from_ipv4_numeric( route_prefix ).ip() << "/"
       << static_cast<int>( prefix_length ) << " => " << ( next_hop.has_value() ? next_hop->ip() : "(direct)" )
       << " on interface " << interface_num << "\n";

  route_table.push_back(route_entry {route_prefix, prefix_length, next_hop, interface_num});
}

void Router::route_one_dgram(InternetDatagram& dgram){
  if(dgram.header.ttl <= 1){
    return;
  }
  int size = route_table.size();
  int fix_interface = -1;
  uint8_t longest = 0;
  for(auto i = 0; i<size; i++){
    
    if(route_table[i].prefix_length > longest){
      uint32_t mask = route_table[i].prefix_length == 0 ? 0 : 0xFFFFFFFF << (32 - route_table[i].prefix_length);
      uint32_t net_ip = route_table[i].route_prefix & mask;
      uint32_t tmp = dgram.header.dst & mask;
      if(tmp == net_ip){
        fix_interface = i;
        longest = route_table[i].prefix_length;
      }
    }
  }
  --dgram.header.ttl;
  if(fix_interface == -1){
    interface(0).send_datagram(dgram, route_table[0].next_hop.value());
    return;
  }
  if(route_table[fix_interface].next_hop.has_value()){
    interface(route_table[fix_interface].interface_num).send_datagram(dgram, route_table[fix_interface].next_hop.value());
  } else {
    interface(route_table[fix_interface].interface_num).send_datagram(dgram, Address::from_ipv4_numeric( dgram.header.dst));
  }
}

void Router::route() {
  if(interfaces_.empty()){
    return;
  }
  for(auto & interface : interfaces_){
    optional<InternetDatagram> dgram = interface.maybe_receive();
    if(dgram.has_value()){
      route_one_dgram(dgram.value());
    }
    // while(dgram.has_value()){
    //   route_one_dgram(dgram.value());
    //   dgram = interface.maybe_receive();
    // }
    
  }
}
